# **Strategies to Solve 530 Problems**

General tips:

1. Do the algebra first, do you plug in numbers until the last step
2. No matter how complicated the budget constraint is, ultimately it comes down to y = b + ax

## **Problem Type 1: Finding MRS**

You will be given a utility function $\bold {U}$ and endogenous variables $\bold {x, y, z...}$, exogeneous variables like $\bold {B, \rho, ...}$

**1. Find the marginal utility of x and y, Ux and Uy, do not plug in numbers yet.**          

$$ \bold {U_x = \frac{dU}{dx}}$$                        
$$ \bold{ U_y = \frac{dU}{dy} }$$           

* Ux and Uy are derivatives of U with respect to x, y          


**2. Find the marginal rate of substitution, note that it depends on your left hand side. Do not plug in numbers yet. What you found here will be your MRS formula for the problem.**           
- Solution 1:           
$$ \bold{\frac{\color{red}dy}{dx} = - \frac{U_x}{\color{red}U_y}}$$          
- Solution 2:          
$$ \bold{\frac{dx}{\color{red}dy} = - \frac{\color{red}U_y}{U_x}}$$          


**3. Plug in values of x and y to the obtain numerical values of MRS.**           

**4. Interpretation of MRS. There are two possible ways.**           
- Base off of Solution 1:         
$$ \bold {dy = - \frac{U_x}{U_y} \text{ } \color{red}dx} $$          
To obtain an additional unit of $\color{red}x$, one must give up $\frac{U_x}{U_y}$ unit of y in order to maintain the same level of utility.          
(Graphically, $\color{red}x$ will be on the horizontal axis, y on the vertical axis.)    


- Base off of Solution 2:           
$$ \bold {dx = - \frac{U_y}{U_x} \text{ } \color{red}dy} $$          
To obtain an additional unit of $\color{red}y$, one must give up $\frac{U_y}{U_x}$ unit of x in order to maintain the same level of utility.        
(Graphically, $\color{red}y$ will be on the horizontal axis, x on the vertical axis.)        
<br>

**5. Suppose an exogeneous variable in the utility function changes, let's say, B.**          
- Look at the MRS formula you got in step 3, ask yourself: **when B goes up, is MRS going up or down, or unchange(if B is not even part of your MRS formula)?**
- Incorporate that into the interpretation you found in step 4:

- Solution 1:

    Because B goes up/down, to obtain an additional unit of $\color{red}x$,
    one must give up more/less unit of y in order to maintain the same level of utility.

- Solution 2: 
    Because B goes up/down, to obtain an additional unit of $\color{red}y$,
    one must give up more/less unit of x in order to maintain the same level of utility.

- Special case:

    Because B is not part of MRS, it does not affect the consumer's preference of x and y. 

<br>

## **Problem Type 2: Aligning the slope of the budget constraint with MRS**

You might be given a budget constraint and asked to find the **utility maximized MRS**.
Because the utility maximized MRS should equal to the slope of the budget constraint (BC), you can just use the slope of the budget constraint. 

This too, exists two different solutions. 

- Solution 1: Putting y on the left hand side 

1. Rewirte your budget constraint, express y in terms of x. The final format should be: y = b + mx

$$ \bold {y = \frac{I}{P_y} - \frac{P_x}{P_y}\text{ } \color{red}x} $$    

2. Take the derivative of y with respect to x. Note the similarity between this and MRS. 

$$ \bold{dy = - \frac{P_x}{P_y}\text{ }{\color{red}dx} } $$   

3. Interpretation

    To obtain an additional unit of $\color{red}x$, one must give up $\frac{P_x}{P_y}$ unit of y in order to maintain the same level of utility.          

- Solution 2: Putting x on the left hand side, follow the same step: 

$$ \bold {x = \frac{I}{P_x} - \frac{P_y}{P_x}\text{ } \color{red}y} $$ 
$$ \bold {dx = - \frac{P_y}{P_x} \text{ } \color{red}dy} $$    

- 
    To obtain an additional unit of $\color{red}y$, one must give up $\frac{U_y}{U_x}$ unit of x in order to maintain the same level of utility.   

<br>

## **Problem Type 3: 2-Period Consumption-Leisure Problem with log utility function**

You will be given the utility function:

$$ \bold {U(c_1, c_2, l_1, l_2) = ln(c_1) + ln(l_1) + ln(\frac{c_2}{1 + \rho}) + ln(\frac{l_2}{1+\rho})}$$

Your exogeneous variables: $\bold {a_0, r, \rho, t_1, t_2, w_1, w_2}$

Your endogenous variables: $\bold {c_1, c_2, l_1, l_2}$

and use the Budget Constraint (not given, so memorize this)

$$ \bold {c_1 + \frac{c_2}{1 + r} + (1-t_1)w_1l_1 + \frac{(1-t_2)w_2l_2}{1+r} = (1 + r)a_0 + (1-t_1)w_1 + \frac{(1-t_2)w_2}{1+r}}$$

(LHS is the endogenous side, RHS is the exogeneous side)

**1. Construct the lagrange in the following format:**

$$\bold {L = U(c_1, c_2, l_1, l_2) + \lambda(BC_{exogenous\_side}) {\color{red}-} \lambda(BC_{endogenous\_side})}$$

**2. Find the 4 FOCs (derivatives of $\bold{U}$ with respect to $\bold{c_1, c_2, l_1, l_2}$)**

**3. Rewrite the 4 FOCs to put $\lambda$ on the left-hand side.**

**4. Find the following 3 relationships:**

$$\text{Relation 1: }\bold{\frac{L_{c_1}}{L_{c_2}}} $$ 
$$\text{Relation 2: }\bold{\frac{L_{l_1}}{L_{l_2}}}$$
$$\text{Relation 3: }\bold{\frac{L_{l_2}}{L_{c_2}}}$$

**5. Set the exogeneous side of BC as A (just to be lazy).**

$$ \bold {c_1 + \frac{c_2}{1 + r} + (1-t_1)w_1l_1 + \frac{(1-t_2)w_2l_2}{1+r} = A}$$

**6. Replace all $\bold{c_1}$ using relation 1. Replace all $\bold{l_1}$ using relation 2.**

- Simplification guide: First factor out $\bold{c_2}$ and $\bold{(1-t_2)w_2l_2}$, then factor out $\bold{\frac{2+\rho}{1+r}}$

**7. Replace $\bold{l_2}$ using relation 3.**

**8. Plug in values to calculate $\bold{c_2}$, note how everything in $\bold{c_2}$ now is given.**

**9. Knowing $\bold{c_2}$, use relations 1 and 3 to get the numerical values of $\bold{c_1}$ and $\bold{l_2}$.**

## **Problem Type 4: the effect of changes in an exogeneous variable**

Read it here: https://gitlab.com/winnieliu725/eco530/-/blob/main/problem_type_4.md
