# **Problem Type 4: the effect of changes in an exogeneous variable**

Problems will be like this

Analyze the effect of a change in $\bold{r}$ between $\bold{c_1}$ and $\bold{l_1}$, $\bold{c_2}$ and $\bold{l_2}$, $\bold{c_1}$ and $\bold{c_2}$. 

Analyze the effect of a change in $\bold{w_1}$ between $\bold{c_1}$ and $\bold{l_1}$, $\bold{c_2}$ and $\bold{l_2}$, $\bold{c_1}$ and $\bold{c_2}$. 

...so on. 

Convention: 
- When studying consumption vs. leisure: leisure is always on the horizontal axis (RHS); 
- When studying $\bold{c_1}$ vs. $\bold{c_2}$: $\bold{c_1}$ is always on the horizontal axis (RHS)
### **<font color='red'>Important: Do not use A anymore, use the full budget constraint</font>**


1. Consumption vs. Leisure in Period 1. Rewrite the budget constraint so that $\bold{c_1}$ is on the left hand side, and that the budget constraint is in $\bold{c_1 = b + m\text{ }l_1}$ format. 

2. Consumption vs. Leisure in Period 2. Rewrite the budget constraint so that $\bold{c_2}$ is on the left hand side, and that the budget constraint is in $\bold{c_2 = b + m\text{ }l_2}$ format. 

3. Consumption in Period 1 vs Consumption in Period 2. Rewrite the budget constraint so that $\bold{c_2}$ is on the left hand side, and that the budget constraint is in $\bold{c_2 = b + m\text{ }c_1}$ format. 

4. Find the MRS for each relation. Ask yourself: when the exogenous variable changes (for example, $\bold{r}$), does that affect your MRS (slope), or intercept?

- If only affecting slope: then there is only substitution effect. 

    - **<font color = 'red'>Interpret the substitution effect like Step 5 in Problem Type 1.</font>**
    - Which good will go up? Which will go down?
- If only affecting the intercept: then there is only an income effect. 
    - Will both goods go up or down?

- If both the intercept and the slope are affected: then both substitution and income effects are present.
    - If both effects indicate a postive change in a good, then it's still positive
    - If one indicates a positive change, one indicates a negative, then the net effect on this good is ambiguous. 